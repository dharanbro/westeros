package com.westeros.rgiri.westeros.models;

/**
 * Created by rgiri on 08-01-2017.
 */

public class Battle {
    private String battleName;
    private String attackerKing;
    private String defenderKing;
    private String attackerStatus;

    public Battle(String battleName, String attackerKing, String defenderKing, String attackerStatus){
        this.battleName=battleName;
        this.attackerKing=attackerKing;
        this.defenderKing=defenderKing;
        this.attackerStatus=attackerStatus;
    }

    public String getAttackerKing() {
        return attackerKing;
    }

    public String getAttackerStatus() {
        return attackerStatus;
    }

    public String getBattleName() {
        return battleName;
    }

    public String getDefenderKing() {
        return defenderKing;
    }
}
