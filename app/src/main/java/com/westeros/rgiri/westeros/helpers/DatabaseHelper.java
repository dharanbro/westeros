package com.westeros.rgiri.westeros.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Giridharan on 12/23/2016.
 */


public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper dbInstance;
    private static Context mContext;

    // Database Version
    private static final int DATABASE_VERSION = 3;

    // Database Name
    public static final String DATABASE_NAME = "westeros";

    // Table Names
    public static final String TABLE_WESTEROS_DATA = "tblWesterosData";
    public static final String TABLE_WESTEROS_KINGS = "tblWesterosKings";


    // TABLE_CONTRACT_DATA - column nmaes
    public static final String KEY_WESTEROS_ID = "id";
    public static final String KEY_WESTEROS_NAME = "name";
    public static final String KEY_WESTEROS_YEAR = "year";
    public static final String KEY_WESTEROS_BATTLE_NO = "battle_number";
    public static final String KEY_WESTEROS_ATTACKER_KING = "attacker_king";
    public static final String KEY_WESTEROS_DEFENDER_KING = "defender_king";
    public static final String KEY_WESTEROS_ATTACKER_ONE = "attacker_1";
    public static final String KEY_WESTEROS_ATTACKER_TWO = "attacker_2";
    public static final String KEY_WESTEROS_ATTACKER_THREEE = "attacker_3";
    public static final String KEY_WESTEROS_ATTACKER_FOUR = "attacker_4";
    public static final String KEY_WESTEROS_DEFENDER_ONE = "defender_1";
    public static final String KEY_WESTEROS_DEFENDER_TWO = "defender_2";
    public static final String KEY_WESTEROS_DEFENDER_THREE = "defender_3";
    public static final String KEY_WESTEROS_DEFENDER_FOUR = "defender_4";
    public static final String KEY_WESTEROS_ATTACKER_OUTCOME = "attacker_outcome";
    public static final String KEY_WESTEROS_BATTLE_TYPE = "battle_type";
    public static final String KEY_WESTEROS_MAJOR_DEATH = "major_death";
    public static final String KEY_WESTEROS_MAJOR_CAPTURE = "major_capture";
    public static final String KEY_WESTEROS_ATTACKER_SIZE = "attacker_size";
    public static final String KEY_WESTEROS_DEFEDER_SIZE = "defender_size";
    public static final String KEY_WESTEROS_ATTACKER_COMMANDER = "attacker_commander";
    public static final String KEY_WESTEROS_DEFENDER_COMMANDER = "defender_commander";
    public static final String KEY_WESTEROS_SUMMER = "summer";
    public static final String KEY_WESTEROS_lOCATION= "location";
    public static final String KEY_WESTEROS_REGION= "region";
    public static final String KEY_WESTEROS_NOTE= "note";

    private static final String CREATE_TABLE_WESTEROS_DATA = "CREATE TABLE IF NOT EXISTS "
            + TABLE_WESTEROS_DATA + "("
            + KEY_WESTEROS_ID + " INTEGER PRIMARY KEY, "
            + KEY_WESTEROS_NAME + " TEXT, "
            + KEY_WESTEROS_YEAR + " INTEGER, "
            + KEY_WESTEROS_BATTLE_NO + " INTEGER, "
            + KEY_WESTEROS_ATTACKER_KING + " TEXT, "
            + KEY_WESTEROS_DEFENDER_KING + " TEXT, "
            + KEY_WESTEROS_ATTACKER_ONE + " TEXT, "
            + KEY_WESTEROS_ATTACKER_TWO + " TEXT, "
            + KEY_WESTEROS_ATTACKER_THREEE + " TEXT, "
            + KEY_WESTEROS_ATTACKER_FOUR + " TEXT,"
            + KEY_WESTEROS_DEFENDER_ONE + " TEXT,"
            + KEY_WESTEROS_DEFENDER_TWO +" TEXT, "
            + KEY_WESTEROS_DEFENDER_THREE + " TEXT, "
            + KEY_WESTEROS_DEFENDER_FOUR + " TEXT, "
            + KEY_WESTEROS_ATTACKER_OUTCOME + " TEXT, "
            + KEY_WESTEROS_BATTLE_TYPE + " TEXT, "
            + KEY_WESTEROS_MAJOR_DEATH + " INTEGER, "
            + KEY_WESTEROS_MAJOR_CAPTURE + " INTEGER,"
            + KEY_WESTEROS_ATTACKER_SIZE + " INTEGER,"
            + KEY_WESTEROS_DEFEDER_SIZE +" INTEGER, "
            + KEY_WESTEROS_ATTACKER_COMMANDER +" TEXT, "
            + KEY_WESTEROS_DEFENDER_COMMANDER + " TEXT, "
            + KEY_WESTEROS_SUMMER + " INTEGER, "
            + KEY_WESTEROS_lOCATION + " TEXT, "
            + KEY_WESTEROS_REGION + " TEXT, "
            + KEY_WESTEROS_NOTE + " TEXT "+
            ")";

    // TABLE_CONTRACT_DATA - column nmaes
    public static final String KEY_WESTEROS_KINGS_ID = "id";
    public static final String KEY_WESTEROS_KINGS_NAME = "name";
    public static final String KEY_WESTEROS_KINGS_RATING = "rating";
    public static final String KEY_WESTEROS_KINGS_BATTLE_COUNT = "battleCount";

    private static final String CREATE_TABLE_WESTEROS_KINGS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_WESTEROS_KINGS + "("
            + KEY_WESTEROS_KINGS_ID + " INTEGER PRIMARY KEY, "
            + KEY_WESTEROS_KINGS_NAME + " TEXT, "
            + KEY_WESTEROS_KINGS_BATTLE_COUNT + " INTEGER, "
            + KEY_WESTEROS_KINGS_RATING + " INTEGER "+
            ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mContext = context;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_WESTEROS_DATA);
        db.execSQL(CREATE_TABLE_WESTEROS_KINGS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // On upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WESTEROS_DATA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WESTEROS_KINGS);

        // Create new tables
        onCreate(db);
    }

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (dbInstance == null) {
            dbInstance = new DatabaseHelper(context);
            Log.d("DatabaseInstance","Null");
        }else{
            Log.d("DatabaseInstance","Available");
        }
        return dbInstance;
    }

    //Get all data in a table
    public Cursor getData(String tableName) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "SELECT * FROM "+tableName,null );
        return res;
    }

    //Get single row in a table with respect to id
    public Cursor getData(String tableName,int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "SELECT * FROM "+tableName+" WHERE id="+id+"", null );
        return res;
    }

    //Get all/single data matches the condition
    public Cursor getData(String tableName,HashMap<String,String> whereCondition) {
        SQLiteDatabase db = this.getReadableDatabase();

        String[] whereConArray = new String[whereCondition.size()];

        Iterator it = whereCondition.entrySet().iterator();
        int i=0;
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            whereConArray[i] = pair.getKey().toString()+"=\""+pair.getValue().toString()+"\"";
            i++;
        }
        String whereCon = TextUtils.join(" AND ",whereConArray);
        String sql="SELECT * FROM "+tableName+" WHERE "+whereCon;
        Log.d("SQL TEST",sql);
        Cursor res =  db.rawQuery( sql, null );
        return res;
    }

    public int numberOfRows(String tableName){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, tableName);
        return numRows;
    }

    public Integer truncate (String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(tableName,null,null);
    }

    public boolean insert(String tableName,HashMap<String,String> hm){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        Iterator it=hm.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            contentValues.put(pair.getKey().toString(), pair.getValue().toString());
            it.remove(); // avoids a ConcurrentModificationException
        }
        if(db.insert(tableName, null, contentValues)>=0){
            Log.d("blacky",contentValues.toString());
            return true;
        }else{
            return false;
        }
    }

    public String delete (String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return String.valueOf(db.delete("tblDemandData","cont_id = ? ",new String[] { id }));
    }


}
