package com.westeros.rgiri.westeros;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.westeros.rgiri.westeros.adapters.BattlesAdapterRC;
import com.westeros.rgiri.westeros.helpers.DatabaseHelper;
import com.westeros.rgiri.westeros.models.Battle;

import java.util.ArrayList;
import java.util.List;

public class KingDetailsActivity extends AppCompatActivity {
    TextView tv_kingsName,tv_won,tv_lost;
    RecyclerView battles_rc;
    BattlesAdapterRC battleAdapter;
    List<Battle> battles;
    LinearLayoutManager mLayoutManager;
    int totalBattles,battlesWon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_king_details);

        tv_kingsName=(TextView)findViewById(R.id.king_name_indvi);
        tv_won=(TextView)findViewById(R.id.won);
        tv_lost=(TextView)findViewById(R.id.lost);
        Intent invoker=getIntent();
        String kingName=invoker.getStringExtra("king_name");
        tv_kingsName.setText(kingName.toString());

        battles=getBattlesOKing(kingName);

        battles_rc=(RecyclerView)findViewById(R.id.battle_list);

        mLayoutManager = new LinearLayoutManager(this);
        battles_rc.setLayoutManager(mLayoutManager);

        battleAdapter=new BattlesAdapterRC(battles,this);
        battles_rc.setAdapter(battleAdapter);

        battleAdapter.notifyDataSetChanged();

    }

    private List<Battle> getBattlesOKing(String kingName) {
        List<Battle> battleList=new ArrayList<Battle>();
        String sql="SELECT * FROM "+ DatabaseHelper.TABLE_WESTEROS_DATA
                +" WHERE "+ DatabaseHelper.KEY_WESTEROS_ATTACKER_KING + " =\""+ kingName +"\" OR "
                + DatabaseHelper.KEY_WESTEROS_DEFENDER_KING + " =\""+ kingName +"\"";
        //String sql="SELECT * FROM "+ DatabaseHelper.TABLE_WESTEROS_DATA;

        Log.d("sql_inspect",sql);
        //Cursor rows=DatabaseHelper.getInstance(getApplicationContext()).getData(DatabaseHelper.TABLE_WESTEROS_DATA);

        Cursor rows=DatabaseHelper.getInstance(getApplicationContext()).getReadableDatabase().rawQuery(sql, null);
        Log.d("sql_inspect_result",String.valueOf(rows.getCount()));
        if(rows.moveToFirst()){
            while(!rows.isAfterLast()){
                totalBattles++;

                String battleName=rows.getString(rows.getColumnIndex(DatabaseHelper.KEY_WESTEROS_NAME));
                String attackerKing=rows.getString(rows.getColumnIndex(DatabaseHelper.KEY_WESTEROS_ATTACKER_KING));
                String defenderKing=rows.getString(rows.getColumnIndex(DatabaseHelper.KEY_WESTEROS_DEFENDER_KING));
                String attackerStatus=rows.getString(rows.getColumnIndex(DatabaseHelper.KEY_WESTEROS_ATTACKER_OUTCOME));

                if (attackerKing.equals(kingName)) {
                    if(attackerStatus.equals("win")){
                        battlesWon++;
                    }
                }
                if(defenderKing.equals(kingName)){
                    if(attackerStatus.equals("loss")){
                        battlesWon++;
                    }
                }

                battleList.add(new Battle(battleName,attackerKing,defenderKing,attackerStatus));
                rows.moveToNext();
            }
        }
        tv_won.setText(String.valueOf(battlesWon));
        tv_lost.setText(String.valueOf(totalBattles-battlesWon));
        Log.d("list_inspect_count",String.valueOf(battleList.size()));
        return battleList;
    }
}
