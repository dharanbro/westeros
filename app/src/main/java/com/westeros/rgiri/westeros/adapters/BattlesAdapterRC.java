package com.westeros.rgiri.westeros.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.westeros.rgiri.westeros.KingDetailsActivity;
import com.westeros.rgiri.westeros.R;
import com.westeros.rgiri.westeros.models.Battle;

import java.util.List;

/**
 * Created by rgiri on 08-01-2017.
 */
public class BattlesAdapterRC extends RecyclerView.Adapter<BattlesAdapterRC.ViewHolder> {
    private List<Battle> mDataset;
    private Context mContext;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View mView;
        public ViewHolder(View v) {
            super(v);
            mView = v;
        }


    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public BattlesAdapterRC(List<Battle> battleList, Context context) {
        mDataset = battleList;
        Log.d("battles_count",String.valueOf(battleList.size()));
        mContext = context;
    }



    // Create new views (invoked by the layout manager)
    @Override
    public BattlesAdapterRC.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rc_item_battles, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        Log.d("Count",String.valueOf(position));

        ((TextView)holder.mView.findViewById(R.id.battle_name)).setText(mDataset.get(position).getBattleName());
        ((TextView)holder.mView.findViewById(R.id.attacker_name)).setText(mDataset.get(position).getAttackerKing());
        ((TextView)holder.mView.findViewById(R.id.defender_name)).setText(mDataset.get(position).getDefenderKing());

    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
