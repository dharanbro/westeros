package com.westeros.rgiri.westeros.models;

/**
 * Created by rgiri on 08-01-2017.
 */

public class King {
    private String kingName;
    private int rating;
    private int battleCount;

    public King(String kingName, int rating, int battleCount){
        this.kingName=kingName;
        this.rating=rating;
        this.battleCount=battleCount;
    }

    public String getKingName() {
        return kingName;
    }

    public int getBattleCount() {
        return battleCount;
    }

    public int getRating() {
        return rating;
    }

}
