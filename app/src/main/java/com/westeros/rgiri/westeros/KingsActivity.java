package com.westeros.rgiri.westeros;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.westeros.rgiri.westeros.adapters.KingsAdapterRC;
import com.westeros.rgiri.westeros.helpers.DatabaseHelper;
import com.westeros.rgiri.westeros.helpers.HttpRequestHandler;
import com.westeros.rgiri.westeros.models.King;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class KingsActivity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    List<King> kingList=new ArrayList<King>();
    Context mContext;
    TextView kingsCount_tv,battleCount_tv;
    int kingsCount=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("method_track","onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        kingsCount_tv=(TextView)findViewById(R.id.kingsCount);
        battleCount_tv=(TextView)findViewById(R.id.battleCount);
        SystemBarTintManager tintManager = new SystemBarTintManager(this);

        tintManager.setTintColor(Color.parseColor("#0E100D"));
        tintManager.setStatusBarTintEnabled(true);

        Window window = KingsActivity.this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(KingsActivity.this, R.color.colorBlack));
        }
        mContext=this.getApplicationContext();

        mRecyclerView = (RecyclerView)findViewById(R.id.my_recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        downloadData();

    }



    @Override
    protected void onStart() {
        super.onStart();

    }

    public void downloadData(){
        // Get a RequestQueue
        RequestQueue queue = HttpRequestHandler.getInstance(this.getApplicationContext()).
                getRequestQueue();

        String url ="http://starlord.hackerearth.com/gotjson";
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        new DownloadDataAsync().execute(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("result_check","Error");
                //mTextView.setText("That didn't work!");
            }
        });

        // Add a request to RequestQueue.
        HttpRequestHandler.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }
    protected void loadToDb(String jsonResponse){
        DatabaseHelper.getInstance(this.getApplicationContext()).truncate(DatabaseHelper.TABLE_WESTEROS_DATA);
        try {
            JSONArray jsonArray = new JSONArray(jsonResponse);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject jsonObj = (JSONObject)jsonArray.get(i);
                Iterator<String> iter = jsonObj.keys();
                HashMap<String,String> fieldVales=new HashMap<String, String>();
                while (iter.hasNext()) {
                    String key = iter.next();
                    try {
                        Object value = jsonObj.get(key);
                        fieldVales.put(key,value.toString());

                    } catch (JSONException e) {
                        // Something went wrong!
                        Log.e("loadTodb",e.toString());
                    }
                }
                DatabaseHelper.getInstance(this.getApplicationContext()).insert(DatabaseHelper.TABLE_WESTEROS_DATA,fieldVales);
            }

        } catch (Throwable t) {
            //Log.e("My App", "Could not parse malformed JSON: \"" + json + "\"");
        }
    }
    protected void populateList(){
        List<King> kingList=new ArrayList<King>();
        String[] kings;

        String sql="SELECT DISTINCT "+DatabaseHelper.KEY_WESTEROS_ATTACKER_KING+ " FROM "+ DatabaseHelper.TABLE_WESTEROS_DATA
                +" WHERE "+ DatabaseHelper.KEY_WESTEROS_ATTACKER_KING +" <> ''";
        Cursor rows=DatabaseHelper.getInstance(this.getApplicationContext()).getReadableDatabase().rawQuery(sql, null);
        kingsCount+=rows.getCount();



        String sql2="SELECT DISTINCT "+DatabaseHelper.KEY_WESTEROS_DEFENDER_KING + " FROM "+ DatabaseHelper.TABLE_WESTEROS_DATA
                + " WHERE "+ DatabaseHelper.KEY_WESTEROS_DEFENDER_KING + " NOT IN ( "+sql+ " ) AND "
                + DatabaseHelper.KEY_WESTEROS_DEFENDER_KING +" <> ''";

        Cursor rows2=DatabaseHelper.getInstance(this.getApplicationContext()).getReadableDatabase().rawQuery(sql2, null);
        kingsCount+=rows2.getCount();

        kings=new String[kingsCount];
        int i=0;

        if (rows.moveToFirst()) {
            while (!rows.isAfterLast()) {
                //your code to implement
                kings[i]=rows.getString(rows.getColumnIndex(DatabaseHelper.KEY_WESTEROS_ATTACKER_KING));
                i++;
                rows.moveToNext();
            }
        }
        rows.close();

        if (rows2.moveToFirst()) {
            while (!rows2.isAfterLast()) {
                //your code to implement
                kings[i]=rows2.getString(rows2.getColumnIndex(DatabaseHelper.KEY_WESTEROS_DEFENDER_KING));
                i++;
                rows2.moveToNext();
            }
        }
        rows2.close();

        kingsCount_tv.setText(String.valueOf(kingsCount));
        DatabaseHelper.getInstance(this.getApplicationContext()).truncate(DatabaseHelper.TABLE_WESTEROS_KINGS);
        for(i=0;i<kingsCount;i++){
            HashMap<String,String> fieldValues=new HashMap<String, String>();
            fieldValues.put(DatabaseHelper.KEY_WESTEROS_KINGS_NAME,kings[i]);
            fieldValues.put(DatabaseHelper.KEY_WESTEROS_KINGS_RATING,"400");
            fieldValues.put(DatabaseHelper.KEY_WESTEROS_KINGS_BATTLE_COUNT,"0");
            DatabaseHelper.getInstance(this.getApplicationContext()).insert(DatabaseHelper.TABLE_WESTEROS_KINGS,fieldValues);
        }
        int battleCount=DatabaseHelper.getInstance(getApplicationContext()).numberOfRows(DatabaseHelper.TABLE_WESTEROS_DATA);
        battleCount_tv.setText(String.valueOf(battleCount));
    }


    protected void calculateRating(){
        Cursor battles_cur=DatabaseHelper.getInstance(this.getApplicationContext()).getData(DatabaseHelper.TABLE_WESTEROS_DATA);

        if(battles_cur.moveToFirst()){
            while(!battles_cur.isAfterLast()){
                String attackingKing=battles_cur.getString(battles_cur.getColumnIndex(DatabaseHelper.KEY_WESTEROS_ATTACKER_KING));
                String defendingKing=battles_cur.getString(battles_cur.getColumnIndex(DatabaseHelper.KEY_WESTEROS_DEFENDER_KING));


                if(!attackingKing.equals("") && !defendingKing.equals("")){

                    HashMap<String,String> whereConDfk=new HashMap<String,String>();
                    whereConDfk.put(DatabaseHelper.KEY_WESTEROS_KINGS_NAME,defendingKing);
                    Cursor cursor1=DatabaseHelper.getInstance(this.getApplicationContext()).getData(DatabaseHelper.TABLE_WESTEROS_KINGS,whereConDfk);
                    double defKing_rating;
                    double defKing_battleCount;
                    if(cursor1.moveToFirst()){

                        defKing_rating=cursor1.getDouble(cursor1.getColumnIndex(DatabaseHelper.KEY_WESTEROS_KINGS_RATING));
                        defKing_battleCount=cursor1.getDouble(cursor1.getColumnIndex(DatabaseHelper.KEY_WESTEROS_KINGS_BATTLE_COUNT));
                        cursor1.close();

                        HashMap<String,String> whereConAtk=new HashMap<String,String>();
                        whereConAtk.put(DatabaseHelper.KEY_WESTEROS_KINGS_NAME,attackingKing);
                        Cursor cursor2=DatabaseHelper.getInstance(this.getApplicationContext()).getData(DatabaseHelper.TABLE_WESTEROS_KINGS,whereConAtk);
                        Double atkKing_rating;
                        Double atkKing_battleCount;
                        if(cursor2.moveToFirst()){

                            atkKing_rating=cursor2.getDouble(cursor2.getColumnIndex(DatabaseHelper.KEY_WESTEROS_KINGS_RATING));
                            atkKing_battleCount=cursor2.getDouble(cursor2.getColumnIndex(DatabaseHelper.KEY_WESTEROS_KINGS_BATTLE_COUNT));
                            cursor2.close();
                            atkKing_battleCount++;
                            defKing_battleCount++;

                            Double defKing_rating_tr=Math.pow(10,(defKing_rating/400));
                            Double atkKing_rating_tr=Math.pow(10,(atkKing_rating/400));

                            Double defKing_rating_ex=defKing_rating_tr/(defKing_rating_tr+atkKing_rating_tr);
                            Double atkKing_rating_ex=atkKing_rating_tr/(defKing_rating_tr+atkKing_rating_tr);

                            String attackerStatus=battles_cur.getString(battles_cur.getColumnIndex(DatabaseHelper.KEY_WESTEROS_ATTACKER_OUTCOME));

                            Double atkKing_rating_new=atkKing_rating;
                            Double defKing_rating_new=defKing_rating;
                            if(attackerStatus.equals("win")){
                                atkKing_rating_new=atkKing_rating+(32*(1-atkKing_rating_ex));
                                defKing_rating_new=defKing_rating+(32*(0-defKing_rating_ex));
                            }else if(attackerStatus.equals("loss")){
                                atkKing_rating_new=atkKing_rating+(32*(0-atkKing_rating_ex));
                                defKing_rating_new=defKing_rating+(32*(1-defKing_rating_ex));
                            }else if(attackerStatus.equals("draw")){
                                atkKing_rating_new=atkKing_rating+(32*(0.5-atkKing_rating_ex));
                                defKing_rating_new=defKing_rating+(32*(0.5-defKing_rating_ex));
                            }
                            String update_atkKing_ratingQuery="UPDATE "+ DatabaseHelper.TABLE_WESTEROS_KINGS + " SET "
                                    + DatabaseHelper.KEY_WESTEROS_KINGS_RATING+" = "+atkKing_rating_new+", "
                                    + DatabaseHelper.KEY_WESTEROS_KINGS_BATTLE_COUNT+" = "+atkKing_battleCount
                                    + " WHERE "+ DatabaseHelper.KEY_WESTEROS_KINGS_NAME +" =\""+attackingKing+"\"";

                            String update_defKing_ratingQuery="UPDATE "+ DatabaseHelper.TABLE_WESTEROS_KINGS + " SET "
                                    + DatabaseHelper.KEY_WESTEROS_KINGS_RATING+" = "+defKing_rating_new+", "
                                    + DatabaseHelper.KEY_WESTEROS_KINGS_BATTLE_COUNT+" = "+defKing_battleCount
                                    + " WHERE "+ DatabaseHelper.KEY_WESTEROS_KINGS_NAME +" =\""+defendingKing+"\"";

                            DatabaseHelper.getInstance(this.getApplicationContext()).getWritableDatabase().execSQL(update_atkKing_ratingQuery);
                            DatabaseHelper.getInstance(this.getApplicationContext()).getWritableDatabase().execSQL(update_defKing_ratingQuery);

                        }

                    }
                }
                battles_cur.moveToNext();
            }
        }

    }
    protected void logRatings(){
        Log.d("method_track","logratings");
        Cursor kings_cur=DatabaseHelper.getInstance(this.getApplicationContext()).getData(DatabaseHelper.TABLE_WESTEROS_KINGS);
        try {
            while (kings_cur.moveToNext()) {

                String name=kings_cur.getString(kings_cur.getColumnIndex(DatabaseHelper.KEY_WESTEROS_KINGS_NAME));
                String rating=kings_cur.getString(kings_cur.getColumnIndex(DatabaseHelper.KEY_WESTEROS_KINGS_RATING));
                String battleCount=kings_cur.getString(kings_cur.getColumnIndex(DatabaseHelper.KEY_WESTEROS_KINGS_BATTLE_COUNT));
                //Log.d("method_track", "logratings");
                Log.d("rating_inspect",name+" - "+rating+" - "+battleCount);

            }
        }finally {
            kings_cur.close();
        }

    }


    public class DownloadDataAsync extends AsyncTask<String, Integer, Context> {

        @Override
        protected Context doInBackground(String... json) {
            loadToDb(json[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Context result) {
            populateList();
            calculateRating();

            Cursor kingsRows=DatabaseHelper.getInstance(getApplicationContext()).getData(DatabaseHelper.TABLE_WESTEROS_KINGS);
            while(kingsRows.moveToNext()){
                String kingName=kingsRows.getString(kingsRows.getColumnIndex(DatabaseHelper.KEY_WESTEROS_KINGS_NAME));
                int battleCount=kingsRows.getInt(kingsRows.getColumnIndex(DatabaseHelper.KEY_WESTEROS_KINGS_BATTLE_COUNT));
                int rating=kingsRows.getInt(kingsRows.getColumnIndex(DatabaseHelper.KEY_WESTEROS_KINGS_RATING));
                kingList.add(new King(kingName,rating,battleCount));

            }
            kingsRows.close();
            mAdapter = new KingsAdapterRC(kingList,mContext);

            mRecyclerView.setAdapter(mAdapter);

            logRatings();

        }
    }




}
